rust-tesseract-sys (0.6.1-6) unstable; urgency=medium

  * add metadata about upstream project
  * update git-buildpackage config:
    + filter out debian subdir
    + improve usage comment
  * update watch file:
    + improve filename and upstream version mangling
    + use Github API

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 08 Feb 2025 00:57:44 +0100

rust-tesseract-sys (0.6.1-5) unstable; urgency=medium

  * tighten (build-)dependency for crate bindgen

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 30 Jan 2025 17:56:34 +0100

rust-tesseract-sys (0.6.1-4) unstable; urgency=medium

  * relax patch 1001 to cover newer branch;
    relax (build-)dependency for crate bindgen;
    closes: bug#1094203, thanks to NoisyCoil
  * update copyright info: update coverage

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 26 Jan 2025 12:15:40 +0100

rust-tesseract-sys (0.6.1-3) unstable; urgency=medium

  * autopkgtest-depend on dh-rust (not dh-cargo)
  * stop mention dh-cargo in long description
  * declare crate-related build-dependencies unconditionally,
    i.e. drop broken nocheck annotations
  * update patch 1001 to cover newer branches of crate bindgen;
    tighten build-dependency for crate bindgen;
    closes: bug#1087403, thanks to NoisyCoil

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 23 Nov 2024 14:26:34 +0100

rust-tesseract-sys (0.6.1-2) unstable; urgency=medium

  * simplify packaging;
    build-depend on dh-sequence-rust
    (not dh-cargo libstring-shellquote-perl)

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 12 Jul 2024 18:27:16 +0200

rust-tesseract-sys (0.6.1-1) unstable; urgency=medium

  [ upstream ]
  * new release(s)

  [ Jonas Smedegaard ]
  * update dh-cargo fork
  * update copyright info: update coverage
  * bump project versions in virtual packages and autopkgtests
  * declare compliance with Debian Policy 4.7.0
  * relax to build- and autopkgtest-depend unversioned
    when version is satisfied in Debian stable
  * tighten (build-)dependency for crate bindgen
  * update and unfuzz patches

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 13 Jun 2024 15:14:32 +0200

rust-tesseract-sys (0.5.14-4) unstable; urgency=medium

  * extend dependency to match newer crate bindgen 0.66.1;
    relax (build-)dependency for crate bindgen;
    closes: bug#1051244, thanks to Peter Green

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 05 Sep 2023 06:41:15 +0200

rust-tesseract-sys (0.5.14-3) unstable; urgency=medium

  * update DEP-3 patch headers
  * update dh-cargo fork;
    closes: bug#1046872, thanks to Lucas Nussbaum

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 20 Aug 2023 11:21:50 +0200

rust-tesseract-sys (0.5.14-2) unstable; urgency=medium

  * tighten autopkgtests
  * stop superfluously provide
    virtual un- or zero-versioned feature packages
  * change binary library package to be arch-independent
  * declare compliance with Debian Policy 4.6.2
  * update dh-cargo fork;
    build-depend on libstring-shellquote-perl
  * update copyright info: update coverage

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 03 Feb 2023 10:54:32 +0100

rust-tesseract-sys (0.5.14-1) unstable; urgency=medium

  [ upstream ]
  * new release

  [ Jonas Smedegaard ]
  * bump version for provided virtual packages and autopkgtest hints

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 02 Dec 2022 20:48:29 +0100

rust-tesseract-sys (0.5.13-1) unstable; urgency=medium

  [ upstream ]
  * new release

  [ Jonas Smedegaard ]
  * drop patch 2001, obsoleted by upstream changes
  * (build-)depend on librust-bindgen-0.60+default-dev
    (not librust-bindgen-0+default-dev)
  * add patch 2002 to avoid windows-only crates;
    stop (build-)depend on librust-vcpkg-0.2+default-dev
  * update dh-cargo fork
  * stop overzealously provide versioned virtual packages
  * update virtual packages and autopkgtests
    to match new upstream release version
  * tighten autopkgtest dependencies

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 24 Oct 2022 17:20:00 +0200

rust-tesseract-sys (0.5.12-3) unstable; urgency=medium

  * fix have autopkgtests depend on tesseract-ocr-eng
  * update copyright info: fix use License shortname public-domain

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 30 Jul 2022 16:14:05 +0200

rust-tesseract-sys (0.5.12-2) unstable; urgency=medium

  * fix depend (not only build-depend) on libtesseract-dev;
    fix stop build-depend on libleptonica-dev

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 08 Jul 2022 20:33:58 +0200

rust-tesseract-sys (0.5.12-1) unstable; urgency=medium

  * initial packaging release;
    closes: bug#1014050

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 29 Jun 2022 15:17:52 +0200
